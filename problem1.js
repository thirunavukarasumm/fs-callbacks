/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/


const fs = require("fs");
const path = require("path");


function problemOne() {
    let dirName = "dummyDirectory";
    let dataContent = Array(100).fill(0).map((item) => {
        return {
            1: 100,
            2: 200,
            3: 300
        }
    })
    let files = Array(10).fill(0).map((element, index) => {
        return `${index}.json`;
    });
    createDirectory(files, dirName);
    function createDirectory(files, dirName) {
        fs.mkdir(path.join(__dirname, dirName), (err) => {
            if (err) {
                if (err.code === "EACCES") {
                    console.log("The folder can't be created as the access is denied to that location.");
                } else if (err.code === "EEXIST") {
                    console.log("The folder already exists.");
                }
            } else {
                console.log("Directory created.");
                createFiles(files, deleteFiles)
            }
        })
    }

    function createFiles(files) {
        let successFile = [];
        let errorFile = [];
        for (let index = 0; index < files.length; index++) {
            fs.writeFile(path.join(dirName, files[index]), JSON.stringify(dataContent), (err, data) => {
                if (err) {
                    if (err.code === "EACCES") {
                        console.log("The file can't be created as the access is denied to that location.");
                    } else if (err.code === "EEXIST") {
                        console.log("The file already exists.");
                    }
                    errorFile.push(files[index])
                } else {
                    console.log("files created.");
                    successFile.push(files[index])
                    if (successFile.length + errorFile.length === files.length) {
                        deleteFiles(files);
                    }

                }
            })
        }
    }

    function deleteFiles(files) {
        let count = 0;
        for (let index = 0; index < files.length; index++) {
            fs.unlink(path.join(dirName, files[index]), (err) => {
                if (err) {
                    if (err.code === "ENOENT") {
                        console.log(`Can't find the file - ${files[index]}.`);
                    }
                } else {
                    count += 1;
                    console.log("files deleted", files[index]);
                    if (count === files.length) {
                        fs.rmdir(dirName, (err) => {
                            if (err) {
                                if (err.code === "ENOENT") {
                                    console.log(`Folder doesn't exist - ${dirName}`);
                                }
                            } else {
                                console.log("Folder deleted.");
                            }
                        })
                    }
                }
            })
        }
    }


}

module.exports = problemOne;

