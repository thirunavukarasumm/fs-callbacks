/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/


let fs = require("fs");
let path  = require("path");

function problemTwo() {
    let lipsumFile = "lipsum.txt";
    let fileNameFile = "filenames.txt";
    let sortFile = "sortFile.txt";
    let lowerCaseFile = "lowerCase.txt";
    let upperCaseFile = "upperCase.txt";

    fs.readFile(path.join(__dirname,lipsumFile), (err, data) => {
        if (err) {
            if(err.code === "ENOENT"){
                console.log(`The file doesn't exist - ${lipsumFile}`);
            }
        } else {
            // Reading the lipsum.txt is successful.
            fs.writeFile(path.join(__dirname,upperCaseFile), data.toString().toUpperCase(), (err) => {
                if (err) {
                    if (err.code === "EACCES") {
                        console.log("The file can't be created as the access is denied to that location.");
                    } else if (err.code === "EEXIST") {
                        console.log(`The file already exists. - ${upperCaseFile}`);
                    }
                } else {
                    // Converted the contents to upperCase and stored them in upperCase.txt.
                    fs.writeFile(path.join(__dirname, fileNameFile), upperCaseFile, (err) => {
                        if (err) {
                            if (err.code === "EACCES") {
                                console.log("The file can't be created as the access is denied to that location.");
                            } else if (err.code === "EEXIST") {
                                console.log(`The file already exists. - ${fileNameFile}`);
                            }
                        } else {
                            // console.log("Added upperCase.txt to fileNames.txt.
                            fs.readFile(path.join(__dirname, fileNameFile), (err, data) => {
                                if (err) {
                                    if (err.code === "ENOENT") {
                                        console.log(`The file doesn't exist - ${fileNameFile}`);
                                    }
                                } else {
                                    // upperCase.txt is read from filenames.txt.
                                    fs.readFile(data.toString(), (err, data) => {
                                        if (err) {
                                            if (err.code === "ENOENT") {
                                                console.log(`The file doesn't exist - ${data.toString()}`);
                                            }
                                        } else {
                                            // upperCase.txt is read from filenames.txt.
                                            let resultString = "";
                                            let sentences = data.toString().toLowerCase().split(".")
                                            for(let index = 0;index < sentences.length;index ++){
                                                resultString += sentences[index] + "\n";
                                            }
                                            
                                            fs.writeFile(path.join(__dirname,lowerCaseFile), resultString, (err) => {
                                                if (err) {
                                                    if (err.code === "EACCES") {
                                                        console.log("The file can't be created as the access is denied to that location.");
                                                    } else if (err.code === "EEXIST") {
                                                        console.log(`The file already exists. - ${lowerCaseFile}`);
                                                    }

                                                } else {
                                                    // Contents are converted to lowercase and split into sentences, and wrote into lowerCase.txt.
                                                    fs.appendFile(path.join(__dirname, fileNameFile), `\n${lowerCaseFile}`, (err) => {
                                                        if (err) {
                                                            if (err.code === "EACCES") {
                                                                console.log("The file can't be created as the access is denied to that location.");
                                                            }else{
                                                                throw new Error(err)
                                                            }
                                                            
                                                        } else {
                                                            
                                                            let resultWords = [];
                                                            fs.readFile(path.join(__dirname,fileNameFile), (err, data) => {
                                                                if (err) {
                                                                    if (err.code === "ENOENT") {
                                                                        console.log(`The file doesn't exist - ${fileNameFile}`);
                                                                    }
                                                                } else {
                                                                    // Reading contents from filenames.txt.
                                                                    let array = data.toString().split("\n")
                                                                    let length = array.length
                                                                    for(let index = 0;index < array.length;index++){
                                                                        fs.readFile(path.join(__dirname,array[index]), (err, data) => {
                                                                            if (err) {
                                                                                if (err.code === "EACCES") {
                                                                                    console.log("The file can't be created as the access is denied to that location.");
                                                                                } else {
                                                                                    throw new Error(err)
                                                                                }

                                                                            } else {
                                                                                // Getting the contents to sort.
                                                                                length -= 1;
                                                                                resultWords.push(data.toString().split(" "))
                                                                                if (length === 0) {
                                                                                    let resultString = "";
                                                                                    let resultArray = [].concat(resultWords[0], resultWords[1]).sort((a, b) => {
                                                                                        return a.localeCompare(b);
                                                                                    })
                                                                                    for(let wordIndex = 0;wordIndex<resultArray.length;wordIndex++){
                                                                                        resultString += resultArray[wordIndex] + " ";
                                                                                    }
                                                                                    fs.writeFile(path.join(__dirname,sortFile), resultString, (err, data) => {
                                                                                        if (err) {
                                                                                            if (err.code === "EACCES") {
                                                                                                console.log("The file can't be created as the access is denied to that location.", sortFile);
                                                                                            } else if (err.code === "EEXIST") {
                                                                                                console.log(`The file already exists. - ${sortFile}`);
                                                                                            }
                                                                                        } else {
                                                                                            // Writing the sorted content to sorted.txt
                                                                                            fs.appendFile(path.join(__dirname,fileNameFile), `\n${sortFile}`, (err) => {
                                                                                                if (err) {
                                                                                                    if (err.code === "EACCES") {
                                                                                                        console.log("The file can't be created as the access is denied to that location.");
                                                                                                    } else {
                                                                                                        throw new Error(err)
                                                                                                    }
                                                                                                } else {
                                                                                                    // Appending sorted.txt to filename.txt
                                                                                                    fs.readFile(path.join(__dirname,fileNameFile), (err, data) => {
                                                                                                        let files = data.toString().split("\n")
                                                                                                        let length = files.length
                                                                                                        for(let fileIndex = 0;fileIndex < files.length;fileIndex++){
                                                                                                            fs.unlink(path.join(__dirname,files[fileIndex]), (err) => {
                                                                                                                if (err) {
                                                                                                                    if (err.code === "ENOENT") {
                                                                                                                        console.log(`Can't find the file - ${files[fileIndex]}.`);
                                                                                                                    }
                                                                                                                } else {
                                                                                                                    // Deleting all the files in filename.txt/
                                                                                                                    length -= 1;
                                                                                                                    if (length === 0) {
                                                                                                                        console.log("done");
                                                                                                                    }
                                                                                                                }
                                                                                                            })
                                                                                                        }
                                                                                                        
                                                                                                    })
                                                                                                }
                                                                                            })
                                                                                        }
                                                                                    })
                                                                                }

                                                                            }
                                                                        })
                                                                    }
                                                                    

                                                                }
                                                            })
                                                        }
                                                    });
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    })
}

problemTwo()
// module.exports = problemTwo;
